import argparse
import yaml
import sys

def print_original_recipe(original_recipe_dict_list, output=sys.stdout):
    if original_recipe_dict_list is not None:
        if isinstance(original_recipe_dict_list, dict):
            if len(original_recipe_dict_list) > 0:
                output.write('\nOriginal Recipes:\n\n')
                for ss, ll in original_recipe_dict_list.items():
                    output.write('* [%s](%s)\n'%(ss, ll))
                output.write('\n\n')
        elif isinstance(original_recipe_dict_list, list):
            output.write('\nOriginal Recipes:\n\n')
            for ll in original_recipe_dict_list:
                output.write('* %s'%(ll))
            output.write('\n\n')
        else:
            raise NotImplementedError("original recipe %s unsupported"%(original_recipe_dict_list))

def print_ingredients(ingredients_list, output=sys.stdout):
    if ingredients_list is None:
        return
    output.write('\nIngredients:\n\n')
    for ingredient in ingredients_list:
        if isinstance(ingredient, dict):
            assert len(ingredient) == 1
            for kk, vv in ingredient.items():
                output.write('* %s\n'%(kk))
                assert isinstance(vv, list), 'Only lists supported'
                for sub_vv in vv:
                    output.write('    * %s\n'%(sub_vv))
        else:
            output.write('* %s\n'%(ingredient))
    output.write('\n')

def print_steps(steps_list, output=sys.stdout):
    if steps_list is None:
        return
    output.write('\nSteps:\n\n')
    for ii, step in enumerate(steps_list):
        output.write('%d. %s\n'%(ii, step))
    output.write('\n')


def print_notes(notes_list, output=sys.stdout):
    if notes_list is None:
        return
    if isinstance(notes_list, list):
        output.write('\nNotes:\n\n')
        for note in notes_list:
            output.write('\n\n%s\n\n'%(note))
    elif isinstance(notes_list, dict):
        output.write('\nNotes:\n\n')
        for kk, vv in notes_list.items():
            output.write('%s:\n')
            if isinstance(vv, list):
                for el in vv:
                    output.write('* %s\n'%(el))
            else:
                output.write('%s\n'%(vv))
            output.write('\n')
    else:
        raise NotImplementedError('Unhandled type for notes: %s'%(type(notes_list)))

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file')
    parser.add_argument('output_file')
    args = parser.parse_args()

    with open(args.input_file, 'r') as fid:
        loaded = yaml.safe_load(fid.read())
        valid_tags = ['original_recipe', 'ingredients', 'steps', 'notes', 'tags']
        coerced = {}
        for kk, vv in loaded.items():
            kk = kk.lower().replace(' ', '_')
            if kk not in valid_tags:
                raise RuntimeError('%s is not a valid tag'%(kk))
            coerced[kk] = vv
         
        with open(args.output_file, 'w') as ofid:
            print_original_recipe(coerced.get('original_recipe', None), output=ofid)
            print_ingredients(coerced.get('ingredients', None), output=ofid)
            print_steps(coerced.get('steps', None), output=ofid)
            print_notes(coerced.get('notes', None), output=ofid)

if __name__ == '__main__':
    main()
