import argparse
import glob
import os

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_dir')
    parser.add_argument('input_md')
    parser.add_argument('output_file')
    parser.add_argument('--no-strip-path', action='store_true')
    args = parser.parse_args()

    file_list = sorted(glob.glob(args.input_dir + '/*.yml'))
    file_list.extend(sorted(glob.glob(args.input_md + '/*.md')))
    with open(args.output_file, 'w') as ofid:
        for ff in file_list:
            noext = os.path.splitext(ff)[0]
            bn = os.path.basename(noext).replace('_', ' ')

            if args.no_strip_path:
                link_dest = noext + '.html'
            else:
                link_dest = os.path.basename(noext) + '.html'
            ofid.write('* [%s](%s)\n'%(bn, link_dest))

if __name__ == '__main__':
    main()
