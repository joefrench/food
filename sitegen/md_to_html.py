import argparse
import markdown
from jinja2 import Template

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--template-file', default=None)
    parser.add_argument('input_file')
    parser.add_argument('output_file')
    args = parser.parse_args()

    template = '{{content}}'
    if args.template_file is not None:
        with open(args.template_file, 'r') as fid:
            template = fid.read()
    jtemplate = Template(template)
    with open(args.input_file, 'r') as infid:
        indata = infid.read()
        with open(args.output_file, 'w') as ofid:
            ofid.write(jtemplate.render(content=markdown.markdown(indata)))

if __name__ == '__main__':
    main()
