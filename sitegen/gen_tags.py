import argparse
import glob
import os
import yaml
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--missing-tags-file', default=None)
    parser.add_argument('--no-strip-path', action='store_true')
    parser.add_argument('input_dir')
    parser.add_argument('output_file')
    args = parser.parse_args()
    assert not args.no_strip_path, 'Currently this program always strips the input_dir part of the path'

    missing_tags = []
    with open(args.output_file, 'w') as ofid:
        tag_dict = {}
        for ff in sorted(glob.glob(args.input_dir + '/*.yml')):
            with open(ff, 'r') as fid:
                data = fid.read()
                for ii, vv in enumerate(data):
                    if ord(vv) >= 128:
                        print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
                        print(ff, '---', hex(ord(vv)))
                        print(data[max(ii-20, 0):min(ii+20, len(data))])
                
                loaded = yaml.safe_load(data)
                if 'tags' in loaded:
                    noext = os.path.splitext(ff)[0]
                    bn = os.path.basename(noext)
                    pretty_name = bn.replace('_', ' ')
                    for vv in loaded['tags']:
                        ll = tag_dict.get(vv, [])
                        ll.append('[%s](%s.html)'%(pretty_name, bn))
                        tag_dict[vv] = ll
                else:
                    missing_tags.append(ff)
        for kk in sorted(tag_dict.keys(), key=lambda xx: xx.lower()):
            ofid.write('# %s\n'%(kk))
            ofid.write('\n')
            for vv in tag_dict[kk]:
                ofid.write('* %s\n'%(vv))
            ofid.write('\n')

    if args.missing_tags_file is not None:
        with open(args.missing_tags_file, 'w') as fid:
            json.dump(missing_tags, fid)

if __name__ == '__main__':
    main()
