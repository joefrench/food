* 100 gr carrot ~1
* 100 gr celery ~1/3-1/2 bunch
* 100 gr onion ~1
* 50 gr shallot
* 100 gr leaks -- light green / white parts ~.5-1 small
* 15 gr garlic
* 5 gr salt
* 2 tbsp olive oil
* black pepper

Sweat for 5-10 minutes until onions are translucent. Makes 400 gr mirepoix
