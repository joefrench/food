* 380 grams (450 ml) rice rinsed
* 600 ml water
* 6 grams salt
* 20 grams oil

1. Rinse rice in pot (weight pot so you know how much extra water to add. want rice + water to weigh 380 + 600 grams)
2. In small pot bring to boil over high heat on smallest burner.
3. Reduce heat to low, simmer for 15 minutes
4. remove from heat and let sit for 5 minutes.

This recipe is the maximum amount for the small pot. 

scale:

* 190 grams rice
* 375 grams water
* 3 grams salt
* 10 grams oil
