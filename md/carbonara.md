Original Recipe: [Serious Eats](https://www.seriouseats.com/pasta-carbonara-sauce-recipe)

* 4 yolks
* 1 whole egg
* 35 gr grated cheese (Parmesan or Romano or a mix)
* 80 gr pancetta
* 250 gr pasta
* 2.0 L water
* 20 gr salt
* 2 TBSP olive oil
* lots of black pepper

1. Measure water + salt, and put on burner over high heat -- (bring to a boil)
2. Put the olive oil in a skillet over medium heat, and add pancetta -- cook until crispy then remove from heat
3. whisk together pepper + eggs + cheese in a bowl that can sit on top of boiling pasta pot
4. When water is boiling add pasta, and cook until tender
5. When pasta is tender add to skillet with pancetta and mix with tongs, and transfer skillet contents to bowl
6. Put bowl over pasta pot with remaining water on low, and stir constantly till it comes together / looks right

