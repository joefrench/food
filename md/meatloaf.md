### Panade:
* 100 gr bread (10 to 1 meat to bread)
* 1 egg
* 1/2 cup cram fraiche (or stock / butter milk / liquid)
* 2 Tbsp gelatin

Work panade into a smooth paste with your hands


### Vegetables 
* 1 medium onion, finely diced (125 grams about 3/4 cup)
* 1 small carrot, peeled and finely diced (60 grams about 1/2 cup)
* 1 stalk celery, finely diced (40 grams about 1/2 cup) 
* 200 grams mushrooms cleaned finely diced
* 3 cloves garlic
* 1 TBsp butter

Add the mushrooms to a pan with a small splash of water, cook until they dry up.
Add butter, salt,and other mirepoix, and sweat.
Add garlic, and cook till fragment

### Glaze

* 1 Heaped TBSP ketchup 
* 1.5 tsp sugar (substituting for 1 TBSP mollasses)
* 1 TBSP red wine vinegar 
* 1 tsp hot sauce 
* 1 tsp wosterchire sauce

### Meatloaf

* 500 grams 80/20 beef
* 500 grams turkey (8%)
* Panade
* Cooked vegetable mixture
* 1 TBSp soy sauce 
* 1 % salt by weight of meat ~ 10 grams
* 120 grams grated cheese

Mix by hand until homogeneous 
Using wet hands form into a loaf on a parchment lined sheet pan
Cover with glaze
Bake till it's 140F in a 350F oven maybe 30-35 minutes 
As run: 175 C Fan  for 45 minutes

2024-aug-04 panade was 50 grams stale bread 100 grams potatoes 150 ml heavy cream and 30 ml stock
