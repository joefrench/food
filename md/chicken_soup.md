* 50 gr pasta
* 40 gr carrot / celery onion mixture
* 80 grams onion
* 50 grams frozen chicken 
* 500 ml /375 grams stock (1 normal package from the freezer)
* 1/2 tsp salt

1. Put it all in a pot heat through and make sure the pasta is cooked. Add additional seasoning (hot sauce, black pepper, etc) to taste.
