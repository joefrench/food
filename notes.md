It would be nice to display recipes in a format where they have the steps and the ingredients next to each other automatically, and that all the ingredients are grouped at the top.

Something like

Ingredients:
* 1/2 onion
* 1 cup peppers
* 2 cups water
* 1/2 lb corn

Steps:
1. Requires: 1/2 onion, 1 cup peppers, 1 Tbsp oil; Action: Sweat the onion and peppers in the oil over low heat
2. Requires: 2 cups water, 1/2 lb corn; Action: Add the water and corn to the onions and peppers, bring to a boil and reduce to a simmer for 20 minutes
3. Requires: N/A; Action: Blend.


To do:
* What data structure makes sense for this? Maybe a list of steps where steps are dictionaries?
* Develop software to render this.
