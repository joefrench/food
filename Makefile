VENVDIR=build/.venv
PYTHON=$(VENVDIR)/bin/python
ENV_CFG=build/.env

$(shell mkdir -p public)
$(shell mkdir -p build)
$(shell $(RM) build/index.md build/tags.md)

YML := $(shell find v1  -maxdepth 1 -name '*.yml' -not -name '*git*')
MD := $(shell find md  -maxdepth 1 -name '*.md' -not -name '*git*')
HTML := $(patsubst v1/%.yml,public/%.html,$(YML)) $(patsubst md/%.md,public/%.html,$(MD)) 

all: public/tags.html public/index.html $(HTML)


$(PYTHON):
	python3 -m virtualenv --python=$(shell which python3) $(VENVDIR)

$(ENV_CFG): sitegen/requirements.txt $(PYTHON)
	$(VENVDIR)/bin/pip install --requirement sitegen/requirements.txt
	touch $@

public/%.html: build/%.md sitegen/md_to_html.py sitegen/template $(PYTHON) $(ENV_CFG)
	$(PYTHON) sitegen/md_to_html.py --template-file=sitegen/template $< $@

public/%.html: md/%.md sitegen/md_to_html.py sitegen/template $(PYTHON) $(ENV_CFG)
	$(PYTHON) sitegen/md_to_html.py --template-file=sitegen/template $< $@

build/index.md: sitegen/gen_index.py $(PYTHON) $(ENV_CFG)
	$(PYTHON) sitegen/gen_index.py v1 md $@

build/tags.md: sitegen/gen_tags.py $(PYTHON) $(ENV_CFG)
	$(PYTHON) sitegen/gen_tags.py v1 $@ --missing-tags-file=missing_tags.log

build/%.md: v1/%.yml sitegen/recipe_yaml_to_md.py $(PYTHON) $(ENV_CFG)
	$(PYTHON) sitegen/recipe_yaml_to_md.py $< $@

clean:
	$(RM) public/*.html build/*.md
distclean:
	$(RM) -r build public

